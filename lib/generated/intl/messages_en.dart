// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "btnHome" : MessageLookupByLibrary.simpleMessage("Explore The Finca"),
    "fincaExteriorTitulo" : MessageLookupByLibrary.simpleMessage("The building"),
    "fincaInteriorTitulo" : MessageLookupByLibrary.simpleMessage("Inside"),
    "fincaTab1" : MessageLookupByLibrary.simpleMessage("Outdoors"),
    "fincaTab2" : MessageLookupByLibrary.simpleMessage("Indoors"),
    "formularioBtnSolicitar" : MessageLookupByLibrary.simpleMessage("Contact us"),
    "formularioDatosTitulo" : MessageLookupByLibrary.simpleMessage("Your Info"),
    "formularioErrorEmail" : MessageLookupByLibrary.simpleMessage("Type a valid email"),
    "formularioErrorMovil" : MessageLookupByLibrary.simpleMessage("Type a valid phone number"),
    "formularioErrorNombre" : MessageLookupByLibrary.simpleMessage("Must be longer than 3 characters"),
    "formularioErrorNroPersonas" : MessageLookupByLibrary.simpleMessage("Must be higher than 0"),
    "formularioHintEmail" : MessageLookupByLibrary.simpleMessage("example@email.com"),
    "formularioLabelApellidos" : MessageLookupByLibrary.simpleMessage("Last Names"),
    "formularioLabelEmail" : MessageLookupByLibrary.simpleMessage("Email"),
    "formularioLabelFechaI" : MessageLookupByLibrary.simpleMessage("Arrive"),
    "formularioLabelFechaS" : MessageLookupByLibrary.simpleMessage("You leave"),
    "formularioLabelMovil" : MessageLookupByLibrary.simpleMessage("Mobile Phone"),
    "formularioLabelNombre" : MessageLookupByLibrary.simpleMessage("Names"),
    "formularioLabelNroPersonas" : MessageLookupByLibrary.simpleMessage("Number of people"),
    "formularioReservaTitulo" : MessageLookupByLibrary.simpleMessage("Booking"),
    "navContacto" : MessageLookupByLibrary.simpleMessage("Contact"),
    "navFinca" : MessageLookupByLibrary.simpleMessage("The Finca"),
    "navInicio" : MessageLookupByLibrary.simpleMessage("Home"),
    "navReserva" : MessageLookupByLibrary.simpleMessage("Book"),
    "navServicio" : MessageLookupByLibrary.simpleMessage("Services")
  };
}
