// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "btnHome" : MessageLookupByLibrary.simpleMessage("Explora La Finca"),
    "fincaExteriorTitulo" : MessageLookupByLibrary.simpleMessage("El Edificio"),
    "fincaInteriorTitulo" : MessageLookupByLibrary.simpleMessage("Por dentro"),
    "fincaTab1" : MessageLookupByLibrary.simpleMessage("El exterior"),
    "fincaTab2" : MessageLookupByLibrary.simpleMessage("Interiores"),
    "formularioBtnSolicitar" : MessageLookupByLibrary.simpleMessage("Solicitar"),
    "formularioDatosTitulo" : MessageLookupByLibrary.simpleMessage("Tus datos"),
    "formularioErrorEmail" : MessageLookupByLibrary.simpleMessage("Ingrese un email válido"),
    "formularioErrorMovil" : MessageLookupByLibrary.simpleMessage("Ingrese un móvil válido"),
    "formularioErrorNombre" : MessageLookupByLibrary.simpleMessage("Ingrese un nombre mayor a 3"),
    "formularioErrorNroPersonas" : MessageLookupByLibrary.simpleMessage("Debe ser mayor a 0"),
    "formularioHintEmail" : MessageLookupByLibrary.simpleMessage("ejemplo@correo.com"),
    "formularioLabelApellidos" : MessageLookupByLibrary.simpleMessage("Apellidos"),
    "formularioLabelEmail" : MessageLookupByLibrary.simpleMessage("Correo Electrónico"),
    "formularioLabelFechaI" : MessageLookupByLibrary.simpleMessage("Llegas"),
    "formularioLabelFechaS" : MessageLookupByLibrary.simpleMessage("Sales"),
    "formularioLabelMovil" : MessageLookupByLibrary.simpleMessage("Teléfono Móvil"),
    "formularioLabelNombre" : MessageLookupByLibrary.simpleMessage("Nombres"),
    "formularioLabelNroPersonas" : MessageLookupByLibrary.simpleMessage("Número de Personas"),
    "formularioReservaTitulo" : MessageLookupByLibrary.simpleMessage("Reserva"),
    "navContacto" : MessageLookupByLibrary.simpleMessage("Contacto"),
    "navFinca" : MessageLookupByLibrary.simpleMessage("Finca"),
    "navInicio" : MessageLookupByLibrary.simpleMessage("Inicio"),
    "navReserva" : MessageLookupByLibrary.simpleMessage("Reserva"),
    "navServicio" : MessageLookupByLibrary.simpleMessage("Servicios")
  };
}
