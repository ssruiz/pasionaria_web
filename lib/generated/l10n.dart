// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

class S {
  S();
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return S();
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  String get navInicio {
    return Intl.message(
      'Inicio',
      name: 'navInicio',
      desc: '',
      args: [],
    );
  }

  String get navServicio {
    return Intl.message(
      'Servicios',
      name: 'navServicio',
      desc: '',
      args: [],
    );
  }

  String get navReserva {
    return Intl.message(
      'Reserva',
      name: 'navReserva',
      desc: '',
      args: [],
    );
  }

  String get navFinca {
    return Intl.message(
      'Finca',
      name: 'navFinca',
      desc: '',
      args: [],
    );
  }

  String get navContacto {
    return Intl.message(
      'Contacto',
      name: 'navContacto',
      desc: '',
      args: [],
    );
  }

  String get btnHome {
    return Intl.message(
      'Explora La Finca',
      name: 'btnHome',
      desc: '',
      args: [],
    );
  }

  String get fincaTab1 {
    return Intl.message(
      'El exterior',
      name: 'fincaTab1',
      desc: '',
      args: [],
    );
  }

  String get fincaTab2 {
    return Intl.message(
      'Interiores',
      name: 'fincaTab2',
      desc: '',
      args: [],
    );
  }

  String get fincaExteriorTitulo {
    return Intl.message(
      'El Edificio',
      name: 'fincaExteriorTitulo',
      desc: '',
      args: [],
    );
  }

  String get fincaInteriorTitulo {
    return Intl.message(
      'Por dentro',
      name: 'fincaInteriorTitulo',
      desc: '',
      args: [],
    );
  }

  String get formularioDatosTitulo {
    return Intl.message(
      'Tus datos',
      name: 'formularioDatosTitulo',
      desc: '',
      args: [],
    );
  }

  String get formularioReservaTitulo {
    return Intl.message(
      'Reserva',
      name: 'formularioReservaTitulo',
      desc: '',
      args: [],
    );
  }

  String get formularioLabelNombre {
    return Intl.message(
      'Nombres',
      name: 'formularioLabelNombre',
      desc: '',
      args: [],
    );
  }

  String get formularioErrorNombre {
    return Intl.message(
      'Ingrese un nombre mayor a 3',
      name: 'formularioErrorNombre',
      desc: '',
      args: [],
    );
  }

  String get formularioLabelApellidos {
    return Intl.message(
      'Apellidos',
      name: 'formularioLabelApellidos',
      desc: '',
      args: [],
    );
  }

  String get formularioLabelEmail {
    return Intl.message(
      'Correo Electrónico',
      name: 'formularioLabelEmail',
      desc: '',
      args: [],
    );
  }

  String get formularioErrorEmail {
    return Intl.message(
      'Ingrese un email válido',
      name: 'formularioErrorEmail',
      desc: '',
      args: [],
    );
  }

  String get formularioHintEmail {
    return Intl.message(
      'ejemplo@correo.com',
      name: 'formularioHintEmail',
      desc: '',
      args: [],
    );
  }

  String get formularioLabelMovil {
    return Intl.message(
      'Teléfono Móvil',
      name: 'formularioLabelMovil',
      desc: '',
      args: [],
    );
  }

  String get formularioErrorMovil {
    return Intl.message(
      'Ingrese un móvil válido',
      name: 'formularioErrorMovil',
      desc: '',
      args: [],
    );
  }

  String get formularioLabelNroPersonas {
    return Intl.message(
      'Número de Personas',
      name: 'formularioLabelNroPersonas',
      desc: '',
      args: [],
    );
  }

  String get formularioErrorNroPersonas {
    return Intl.message(
      'Debe ser mayor a 0',
      name: 'formularioErrorNroPersonas',
      desc: '',
      args: [],
    );
  }

  String get formularioBtnSolicitar {
    return Intl.message(
      'Solicitar',
      name: 'formularioBtnSolicitar',
      desc: '',
      args: [],
    );
  }

  String get formularioLabelFechaI {
    return Intl.message(
      'Llegas',
      name: 'formularioLabelFechaI',
      desc: '',
      args: [],
    );
  }

  String get formularioLabelFechaS {
    return Intl.message(
      'Sales',
      name: 'formularioLabelFechaS',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}