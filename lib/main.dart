import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'locator.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:finca_pasionaria/generated/l10n.dart';

import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/services/navigation_service.dart';
import 'package:finca_pasionaria/src/providers/app_provider.dart';

import 'package:finca_pasionaria/src/pages/main_page.dart';
import 'package:finca_pasionaria/src/routes/router.dart' as router;

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    precacheImage(AssetImage('assets/img/finca/ext/3.jpg'), context);
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AplicacionProvider>(
            create: (_) => new AplicacionProvider(),
          ),
          ChangeNotifierProvider<RutasProvider>(
            create: (_) => new RutasProvider(),
          ),
        ],
        //create: (_) => new AplicacionProvider(),
        child: Consumer2<AplicacionProvider, RutasProvider>(
          builder: (context, value1, value2, child) => MaterialApp(
            // localization
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              S.delegate
            ],

            supportedLocales: S.delegate.supportedLocales,
            locale: Locale(value1.idioma),
            //-----------------------------

            debugShowCheckedModeBanner: false,
            title: 'Pasionaria',
            builder: (context, child) => MainPage(child: child),
            //home: MainPage(),
            navigatorKey: locator<NavigationService>().navigatorKey,
            onGenerateRoute: router.generatedRoute,
            initialRoute: HomeRoute,

            //theme
            theme: ThemeData(
                primaryColor: Colors.orange,
                //accentColor: Colors.orange,
                primarySwatch: Colors.orange),
          ),
        ));
  }
}
