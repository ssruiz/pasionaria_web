import 'package:flutter/material.dart';

class ColorHover extends StatefulWidget {
  final Widget child;
  const ColorHover({Key key, this.child}) : super(key: key);

  @override
  _ColorHoverState createState() => _ColorHoverState();
}

class _ColorHoverState extends State<ColorHover> {
  final colorIn = Colors.orange[700];
  final colorOut = Colors.orange;
   bool _hovering = false;
  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 100),
        child: widget.child,
        color: _hovering ? colorIn : colorOut,
    
      ),
    );
  }

  void _mouseEnter(bool value) {
    setState(() {
      _hovering = value;
    });
  }
}

