import 'package:flutter/material.dart';

class ColorMenuHover extends StatefulWidget {
  final Widget child;
  const ColorMenuHover({Key key, this.child}) : super(key: key);

  @override
  _ColorMenuHoverState createState() => _ColorMenuHoverState();
}

class _ColorMenuHoverState extends State<ColorMenuHover> {
  final colorIn = Colors.orangeAccent;
  final colorOut = Colors.transparent;
   bool _hovering = false;
  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 100),
        child: widget.child,
        color: _hovering ? colorIn : colorOut,
    
      ),
    );
  }

  void _mouseEnter(bool value) {
    setState(() {
      _hovering = value;
    });
  }
}
