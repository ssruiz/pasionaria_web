import 'package:finca_pasionaria/src/util/color_hover/color_hover.dart';
import 'package:finca_pasionaria/src/util/color_hover/color_hover_menu.dart';
import 'package:finca_pasionaria/src/util/translate_hover/tranlaste_hover_w.dart';
import 'package:flutter/material.dart';
import 'dart:html' as html;

extension HoverExtension on Widget{
  static final appContainer = html.document.getElementById('app-container');


  Widget get showCursor{
    return MouseRegion(
      child: this,
      onHover: (event) => appContainer.style.cursor='pointer',
      onExit: (event) => appContainer.style.cursor='default',
    );

  }

  Widget get moveUpOnHover {
    return TranslateOnHover(
      child: this,
    );
  }

  Widget get colorMouseHover{
    return ColorHover(
      child: this,
    );
  }

  Widget get colorHoverMenu{
    return ColorMenuHover(
      child: this,
    );
  }
}