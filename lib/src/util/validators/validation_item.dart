class ValidationItem{
  final dynamic value;
  final bool error;

  ValidationItem(this.value, this.error);
}