import 'dart:io';
import 'package:url_launcher/url_launcher.dart';

lanzarUrl(String url) async {
  try {
    //const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  } catch (e) {}
}

lanzarWhatsapp() async {
  String phone = '';

  String url() {
    if (Platform.isIOS) {
      return "whatsapp://wa.me/$phone/";
      //return "whatsapp://wa.me/$phone/?text=${Uri.parse(message)}";
    } else if (Platform.isAndroid) {
      return "whatsapp://send?phone=$phone}";
    } else {
      return 'https://api.whatsapp.com/send?phone=$phone';
    }
  }

  if (await canLaunch(url())) {
    await launch(url());
  } else {
    throw 'Could not launch ${url()}';
  }
}

lanzarIG() async {
  String phone = '';

  String url() {
    if (Platform.isIOS) {
      return "whatsapp://wa.me/$phone/";
      //return "whatsapp://wa.me/$phone/?text=${Uri.parse(message)}";
    } else if (Platform.isAndroid) {
      return "whatsapp://send?phone=$phone}";
    } else {
      return 'https://api.whatsapp.com/send?phone=$phone';
    }
  }

  if (await canLaunch(url())) {
    await launch(url());
  } else {
    throw 'Could not launch ${url()}';
  }
}
