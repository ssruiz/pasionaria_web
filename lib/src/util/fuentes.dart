

import 'package:google_fonts/google_fonts.dart';
export 'package:google_fonts/google_fonts.dart';

final titleTextStyle = GoogleFonts.playfairDisplaySc();
//final titleTextStyle = GoogleFonts.ptSerif();
final subtitleTextStyle = GoogleFonts.merriweather();