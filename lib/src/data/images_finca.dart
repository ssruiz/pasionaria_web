import 'package:flutter/material.dart';

final List<AssetImage> listadoExterior = [
  AssetImage('assets/img/finca/ext/1.jpg'),
  AssetImage('assets/img/finca/ext/2.jpg'),
  AssetImage('assets/img/finca/ext/3.jpg'),
];

final List<AssetImage> listadoInterior = [
  AssetImage('assets/img/finca/int/1.jpg'),
  AssetImage('assets/img/finca/int/2.jpg'),
  AssetImage('assets/img/finca/int/3.jpg'),
  AssetImage('assets/img/finca/int/4.jpg'),
  AssetImage('assets/img/finca/int/5.jpg'),
];