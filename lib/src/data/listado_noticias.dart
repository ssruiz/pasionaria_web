
final List<_NoticiaModel> listadoNoticiasEs = [
 _NoticiaModel('Notica 1', 'Sunt amet anim eu dolore id. Occaecat exercitation ad cillum irure irure. Amet sunt aute ipsum aliquip in dolore veniam amet velit quis laboris. Voluptate irure tempor minim esse proident ullamco ullamco occaecat.'),
 _NoticiaModel('Notica 2', 'Sunt amet anim eu dolore id. Occaecat exercitation ad cillum irure irure. Amet sunt aute ipsum aliquip in dolore veniam amet velit quis laboris. Voluptate irure tempor minim esse proident ullamco ullamco occaecat.'),

];

final List<_NoticiaModel> listadoNoticiasEn = [
 _NoticiaModel('News 1', 'Sunt amet anim eu dolore id. Occaecat exercitation ad cillum irure irure. Amet sunt aute ipsum aliquip in dolore veniam amet velit quis laboris. Voluptate irure tempor minim esse proident ullamco ullamco occaecat.'),
 _NoticiaModel('News 2', 'Sunt amet anim eu dolore id. Occaecat exercitation ad cillum irure irure. Amet sunt aute ipsum aliquip in dolore veniam amet velit quis laboris. Voluptate irure tempor minim esse proident ullamco ullamco occaecat.'),

];

class _NoticiaModel{
  final String titulo;
  final String noticia;

  _NoticiaModel(this.titulo, this.noticia);
}