import 'package:flutter/material.dart';

final List<_ServicioModel> listadoNoticiasEs = [
 _ServicioModel('Servicio 1', 'Sunt amet anim eu dolore id. Occaecat exercitation ad cillum irure irure. Amet sunt aute ipsum aliquip in dolore veniam amet velit quis laboris. Voluptate irure tempor minim esse proident ullamco ullamco occaecat.'),
 _ServicioModel('Servicio 2', 'Sunt amet anim eu dolore id. Occaecat exercitation ad cillum irure irure. Amet sunt aute ipsum aliquip in dolore veniam amet velit quis laboris. Voluptate irure tempor minim esse proident ullamco ullamco occaecat.'),

];

final List<_ServicioModel> listadoNoticiasEn = [
 _ServicioModel('Service 1', 'Sunt amet anim eu dolore id. Occaecat exercitation ad cillum irure irure. Amet sunt aute ipsum aliquip in dolore veniam amet velit quis laboris. Voluptate irure tempor minim esse proident ullamco ullamco occaecat.'),
 _ServicioModel('Service 2', 'Sunt amet anim eu dolore id. Occaecat exercitation ad cillum irure irure. Amet sunt aute ipsum aliquip in dolore veniam amet velit quis laboris. Voluptate irure tempor minim esse proident ullamco ullamco occaecat.'),

];

final List<AssetImage> listadoImgServicios = [
  AssetImage('assets/img/servicios/1.jpg'),
  AssetImage('assets/img/servicios/2.jpg'),
  AssetImage('assets/img/servicios/4.jpg'),
  AssetImage('assets/img/servicios/4.jpg'),
];

class _ServicioModel{
  final String titulo;
  final String descripcion;

  _ServicioModel(this.titulo, this.descripcion);
}