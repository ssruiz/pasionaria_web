
import 'package:flutter/material.dart';


// final routesApp = <String,_Route>{
//   'inicio' : _Route('home', HomePage()),
//   '/' : _Route('home', HomePage()),
//   'finca' : _Route('finca', FincaPage())
// };

const String HomeRoute = '/inicio';
const String FincaRoute = '/finca';
const String ServiciosRoute = '/servicios';
const String BookRoute = '/reserva';
// class _Route{
//   final String nombre;
//   final Widget ruta;

//   _Route(this.nombre, this.ruta);
// }