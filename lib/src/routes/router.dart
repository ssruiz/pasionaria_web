
import 'package:finca_pasionaria/src/pages/book_page/book_page.dart';
import 'package:finca_pasionaria/src/pages/finca_page/finca_page.dart';
import 'package:finca_pasionaria/src/pages/home_page/home_page.dart';
import 'package:finca_pasionaria/src/pages/services_page/servicios_page.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:flutter/material.dart';

Route<dynamic> generatedRoute(RouteSettings settings) {
  // print('Ruta: ${settings.name}');
  switch (settings.name) {
    case HomeRoute:
      return MaterialPageRoute(
          builder: (context) => HomePage(), settings: settings);
      break;
    case FincaRoute:
      return MaterialPageRoute(
          builder: (context) => FincaPage(), settings: settings);
      break;
    case ServiciosRoute:
      return MaterialPageRoute(
          builder: (context) => ServiciosPage(), settings: settings);
    case BookRoute:
      return MaterialPageRoute(
          builder: (context) => BookPage(), settings: settings);
    default:
      return MaterialPageRoute(
          builder: (context) => HomePage(), settings: settings);
  }
}

PageRoute _getPageRoute(Widget child) {
  return _FadeRoute(
    child: child,
  );
}
class _FadeRoute extends PageRouteBuilder {
  final Widget child;
  _FadeRoute({this.child})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              child,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
        );
}