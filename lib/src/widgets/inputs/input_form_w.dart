import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:flutter/material.dart';
import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:finca_pasionaria/src/providers/form_validation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DatosFormulario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              S.of(context).formularioDatosTitulo,
              style: subtitleTextStyle.copyWith(
                color: Colors.black87,
                fontSize: 25,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  InputNombre(),
                  InputApellido(),
                  InputEmail(),
                  InputMovil(),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Reserva extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              S.of(context).formularioReservaTitulo,
              style: subtitleTextStyle.copyWith(
                color: Colors.black87,
                fontSize: 25,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _DatePicker(
                      fecha: prov.fechaInicio,
                      icono: FontAwesomeIcons.calendarCheck,
                      texto: 'Fecha Entrada'),
                  _DatePicker(
                    fecha: prov.fechaFin,
                    icono: FontAwesomeIcons.calendarMinus,
                    texto: 'Fecha Salida',
                  ),
                  InputNroPersonas(),
                  SizedBox(
                    height: 35,
                  ),
                  BotonSubmit(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BotonSubmit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      height: 40,
      width: double.infinity,
      child: RaisedButton(
        color: Colors.orange,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        onPressed: prov.validateForm ? () {} : null,
        child: Text(
          S.of(context).formularioBtnSolicitar,
          style: subtitleTextStyle.copyWith(fontSize: 15, color: Colors.white),
        ),
      ),
    );
  }
}

class _DatePicker extends StatefulWidget {
  final fecha;
  final IconData icono;
  final String texto;
  const _DatePicker({Key key, this.fecha, this.icono, this.texto})
      : super(key: key);

  @override
  __DatePickerState createState() => __DatePickerState();
}

class __DatePickerState extends State<_DatePicker> {
  TextEditingController cont = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    cont?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 15,
      ),
      child: TextField(
        controller: cont,
      
        decoration: InputDecoration(
            labelText: widget.texto,
            icon: FaIcon(
              FontAwesomeIcons.calendar,
              color: Colors.orange,
            )),
        onTap: () {
          showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(DateTime.now().year),
                  lastDate: DateTime(2100))
              .then((value) {
            if (value != null) {
              prov.changeFechaInicio(value);
              cont.text = '${DateFormat("dd-MM-yyyy").format(value)}';
              setState(() {});
            }
          });
        },
      ),
    );
    // return Container(
    //   //height: 50,

    //   decoration: BoxDecoration(
    //     border: Border.all(color: Colors.grey),
    //     color: Colors.grey[50],
    //     borderRadius: BorderRadius.circular(10),
    //   ),
    //   child: ListTile(
    //     focusColor: Colors.orange,
    //     visualDensity: VisualDensity.compact,
    //     //dense: true,
    //     hoverColor: Colors.red,
    //     leading: Text(
    //       widget.texto,
    //       style: subtitleTextStyle.copyWith(
    //         color: Colors.black,
    //       ),
    //     ),
    //     title: Text(
    //       '${DateFormat("dd-MM-yyyy").format(widget.fecha)}',
    //       style: subtitleTextStyle.copyWith(
    //         color: Colors.black54,
    //       ),
    //       textAlign: TextAlign.center,
    //     ),
    //     trailing: FaIcon(
    //       widget.icono,
    //       color: Colors.orange,
    //     ),
    //     onTap: () {
    //       showDatePicker(
    //               context: context,
    //               initialDate: DateTime.now(),
    //               firstDate: DateTime(DateTime.now().year),
    //               lastDate: DateTime(2100))
    //           .then((value) {
    //         if (value != null) prov.changeFechaInicio(value);
    //       });
    //     },
    //   ),
    // );
  }
}

class InputNombre extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 15,
      ),
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: FaIcon(
            FontAwesomeIcons.idCard,
          ),
          labelText: S.of(context).formularioLabelNombre,
          errorText:
              prov.nombre.error ? S.of(context).formularioErrorNombre : null,
        ),
        onChanged: (value) => prov.changeNombre(value),
        onEditingComplete: () {},
      ),
    );
  }
}

class InputNroPersonas extends StatelessWidget {
  //final TextEditingController c = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 15,
      ),
      child: TextField(
      //  controller: c,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          icon: FaIcon(
            FontAwesomeIcons.userFriends,
          ),
          labelText: S.of(context).formularioLabelNroPersonas,
          errorText: prov.nroPersonas.error
              ? S.of(context).formularioErrorNroPersonas
              : null,
        ),
        onChanged: (value) => prov.changeNroPersonas(value),
        onEditingComplete: () {},
      ),
    );
  }
}

class InputApellido extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 15,
      ),
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: FaIcon(FontAwesomeIcons.idCardAlt),
          labelText: S.of(context).formularioLabelApellidos,
          errorText:
              prov.apellido.error ? S.of(context).formularioErrorNombre : null,
        ),
        onChanged: (value) => prov.changeApellido(value),
      ),
    );
  }
}

class InputEmail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 15,
      ),
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            icon: FaIcon(FontAwesomeIcons.at),
            hintText: S.of(context).formularioHintEmail,
            labelText: S.of(context).formularioLabelEmail,
            errorText:
                prov.email.error ? S.of(context).formularioErrorEmail : null),
        onChanged: (value) => prov.changeEmail(value),
      ),
    );
  }
}

class InputMovil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 15,
      ),
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
            icon: FaIcon(
              FontAwesomeIcons.mobile,
              size: 30,
            ),
            labelText: S.of(context).formularioLabelMovil,
            errorText:
                prov.movil.error ? S.of(context).formularioErrorMovil : null),
        onChanged: (value) => prov.changeMovil(value),
      ),
    );
  }
}
