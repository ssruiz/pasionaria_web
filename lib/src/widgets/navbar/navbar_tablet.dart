import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/widgets/navbar/titulo_w.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:finca_pasionaria/src/providers/app_provider.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:finca_pasionaria/src/widgets/navbar/navbar_item.dart';
import 'package:finca_pasionaria/src/util/hover_extension.dart';

class NavBarTablet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final home = Provider.of<RutasProvider>(context).rutaActual;

    return Row(
      mainAxisAlignment: home != HomeRoute
          ? MainAxisAlignment.spaceBetween
          : MainAxisAlignment.end,
      children: [
        if (home != HomeRoute) Flexible(flex: 1, child: TituloPagina()),
        Flexible(flex: 2, child: _Menu()),
      ],
    );
  }
}

class _Menu extends StatelessWidget {
  const _Menu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: EdgeInsets.all(5),
      height: 100,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(0.5),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: NavBarItem(
              texto: S.of(context).navInicio,
              index: 0,
              navigateToPage: HomeRoute,
            ),
          ),
          Expanded(
            child: NavBarItem(
              texto: S.of(context).navFinca,
              index: 1,
              navigateToPage: FincaRoute,
            ),
          ),
          Expanded(
            child: NavBarItem(
              texto: S.of(context).navServicio,
              index: 2,
              navigateToPage: ServiciosRoute,
            ),
          ),
          Expanded(
            child: NavBarItem(
              texto: S.of(context).navReserva,
              index: 3,
              navigateToPage: BookRoute,
            ),
          ),
          Expanded(flex: 1, child: _CajaIdioma())
        ],
      ),
    );
  }
}

class _CajaIdioma extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final idioma = Provider.of<AplicacionProvider>(context).idioma;
    final styleSelectet = subtitleTextStyle.copyWith(
        color: Colors.orange, fontWeight: FontWeight.bold, fontSize: 15);

    final noSelected = subtitleTextStyle.copyWith(
        color: Colors.white, fontWeight: FontWeight.w100, fontSize: 12);
    return Container(
      // color: Colors.red,
      decoration: BoxDecoration(
          border: Border(left: BorderSide(color: Colors.blueGrey, width: 1))),
      height: double.infinity,
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Spacer(),
          GestureDetector(
              onTap: () {
                Provider.of<AplicacionProvider>(context, listen: false).idioma =
                    'es';
              },
              child: Container(
                  child: Text(
                'Es',
                style: idioma == 'es' ? styleSelectet : noSelected,
              ))).showCursor,
          Spacer(),
          GestureDetector(
              onTap: () {
                Provider.of<AplicacionProvider>(context, listen: false).idioma =
                    'en';
              },
              child: Container(
                  child: Text(
                'En',
                style: idioma == 'en' ? styleSelectet : noSelected,
              ))).showCursor,
          Spacer(),
        ],
      ),
    );
  }
}
