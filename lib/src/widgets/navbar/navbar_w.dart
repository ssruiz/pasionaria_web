import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';



import 'package:finca_pasionaria/src/widgets/navbar/navbar_desktop_w.dart';
import 'package:finca_pasionaria/src/widgets/navbar/navbar_tablet.dart';


class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (BuildContext context) => NavBarDesktop(),
      tablet: (BuildContext context) => NavBarTablet(),
      mobile: (BuildContext context) => Container(color: Colors.blue),
      watch: (BuildContext context) => Container(color: Colors.purple),
    );
  }
}
