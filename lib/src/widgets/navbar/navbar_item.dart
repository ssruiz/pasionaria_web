import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/util/hover_extension.dart';
import 'package:responsive_builder/responsive_builder.dart';

class NavBarItem extends StatelessWidget {
  final String texto;
  final int index;
  final String navigateToPage;
  const NavBarItem(
      {@required this.texto,
      @required this.index,
      @required this.navigateToPage});

  @override
  Widget build(BuildContext context) {
    final provRutas = Provider.of<RutasProvider>(context);
    return ResponsiveBuilder(
      builder: (context, sizeInfo) {

        final textSize = sizeInfo.isDesktop ? 17.0 : 12.0;
        return GestureDetector(
          onTap: () {
            provRutas.index = index;
            provRutas.rutaActual = navigateToPage;
          },
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 3),
                  decoration: BoxDecoration(
                    border: provRutas.index == index
                        ? Border(
                            bottom: BorderSide(color: Colors.orange, width: 2))
                        : null,
                  ),
                  child: Text(
                    texto,
                    style: GoogleFonts.merriweather(
                        color: Colors.white, fontSize: textSize, letterSpacing: 1),
                  ),
                ),
              ],
            ),
          ),
        ).showCursor.moveUpOnHover;
      },
    );
  }
}
