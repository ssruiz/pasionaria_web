import 'package:finca_pasionaria/src/pages/home_page/home_page.dart';
import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/widgets/navbar/titulo_w.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:finca_pasionaria/src/util/hover_extension.dart';
import 'package:provider/provider.dart';

class NavBarMobile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provRutas = Provider.of<RutasProvider>(context);
    final heigthS = MediaQuery.of(context).size.height;
    return Container(
      //padding: EdgeInsets.only(left: 50),
      height: heigthS > 900 ? 100 : 50 ,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(0.5),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: 100,
            child: IconButton(
                icon: FaIcon(FontAwesomeIcons.bars),
                color: Colors.white,
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                }).moveUpOnHover,
          ),
          if (provRutas.rutaActual != HomeRoute) Expanded(child: TituloPagina()),

          // Expanded(child: )
          // NavBarItem(texto: S.of(context).navInicio, index: 0,  navigateToPage: HomeRoute,),
          // NavBarItem(texto:S.of(context).navFinca, index: 1,  navigateToPage: FincaRoute,),
          // NavBarItem(texto:S.of(context).navServicio, index: 2,  navigateToPage: HomeRoute,),
          // NavBarItem(texto: S.of(context).navReserva, index: 3,  navigateToPage:HomeRoute,),
        ],
      ),
    );
  }
}
