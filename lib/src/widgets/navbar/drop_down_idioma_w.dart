import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:flutter/material.dart';

class DropIdioma extends StatefulWidget {
  @override
  _DropIdiomaState createState() => _DropIdiomaState();
}

class _DropIdiomaState extends State<DropIdioma> {
  String idioma = 'es';
  
  @override
  Widget build(BuildContext context) {
    return Container(
        child: DropdownButton(
            value: idioma,
       //style: TextStyle(),
            items: <DropdownMenuItem>[
              DropdownMenuItem(
                  value: 'es',
                  child: Text('Es',
                      style: subtitleTextStyle.copyWith(
                          color: Colors.black, fontSize: 18))),
              DropdownMenuItem(
                  value: 'en',
                  child: Text('En',
                      style: subtitleTextStyle.copyWith(
                          color: Colors.black, fontSize: 18))),
            ],
            onChanged: (value){
              setState(() {
                idioma=value;
              });
            }));
  }
}
