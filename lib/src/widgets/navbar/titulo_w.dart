import 'package:auto_size_text/auto_size_text.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:flutter/material.dart';

class TituloPagina extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxWidth: 400,
        minWidth: 50,
        minHeight: 100
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        height: 100,
        //width: 500,
        decoration: BoxDecoration(color: Colors.orange.withOpacity(0.5)),
        child: Align(
            alignment: Alignment.center,
            child: AutoSizeText(
              'Finca Pasionaria',
              style: titleTextStyle.copyWith(
                  letterSpacing: 2, fontSize: 25, color: Colors.white),
            )),
      ),
    );
  }
}
