import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:finca_pasionaria/src/data/listado_noticias.dart';
import 'package:finca_pasionaria/src/providers/app_provider.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class SliderNoticias extends StatelessWidget {
  final device; // 1 = desktop <> 1 table/movil

  const SliderNoticias({Key key, @required this.device}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //final pageController = new PageController();
    return device == 1 ? _VistaDesktop() : _VistaMobile();
  }
}

class _VistaDesktop extends StatelessWidget {
  //final PageController pageController = new PageController();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => new _SlideShowModel(),
      child: Row(
        //crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Flexible(
              child: _Slides(
            pageController: PageController(),
            size: 16,
          )),
          _CajaNoticiasNav()
        ],
      ),
    );
  }
}

class _VistaMobile extends StatelessWidget {
  //final PageController pageController = new PageController();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => new _SlideShowModel(),
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Expanded(child: _Slides(pageController: PageController(), size: 12)),
          // _Dots(listadoNoticias.length
          //     //pageController: pageController,
          //     )
        ],
      ),
    );
  }
}

class _Dots extends StatelessWidget {
  final int count;
  _Dots(this.count);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      //height: 50,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(count, (i) => _Dot(i))
          //crearDots()
          ),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;

  const _Dot(this.index);

  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<_SlideShowModel>(context);
    double medida;
    Color color;
    if (index - 0.5 <= prov.actual && prov.actual < index + 0.5) {
      medida = prov._bulletP;
      color = prov.colorPrimario;
    } else {
      medida = prov._bulletS;
      color = prov.colorSecundario;
    }
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      height: medida,
      width: medida,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(color: color, shape: BoxShape.circle),
    );
  }
}

class _Slides extends StatefulWidget {
  final PageController pageController;
  final double size;

  const _Slides({Key key, @required this.size, @required this.pageController})
      : super(key: key);
  @override
  __SlidesState createState() => __SlidesState();
}

class __SlidesState extends State<_Slides> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    //print('Controller disposed');
    widget.pageController?.dispose();
    //Provider.of<_SlideShowModel>(context,listen: false).dispose();
    widget.pageController ?? print('YES');
    //print(widget.pageController ?? 'YES');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      final aux = Provider.of<AplicacionProvider>(context).idioma;
      final listadoNoticias =
          aux == 'es' ? listadoNoticiasEs : listadoNoticiasEn;

      Provider.of<_SlideShowModel>(context, listen: false).controller =
          widget.pageController;
      widget.pageController.addListener(() {
        Provider.of<_SlideShowModel>(context, listen: false).actual =
            widget.pageController.page;
        setState(() {});
      });
      return ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
          child: Container(
            decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
            padding: EdgeInsets.only(left: 30, right: 30, top: 30, bottom: 10),
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: PageView(
                    controller: widget.pageController,
                    physics: BouncingScrollPhysics(),
                    children: <Widget>[
                      ...listadoNoticias
                          .map((e) => _NoticiaWidgetModel(
                                noticia: e.noticia,
                                titulo: e.titulo,
                                size: widget.size,
                              ))
                          .toList()
                    ],
                  ),
                ),
                //SizedBox(height: 10,),
                _Dots(listadoNoticias.length
                    //pageController: pageController,
                    )
              ],
            ),
          ),
        ),
      );
    });
  }
}

class _NoticiaWidgetModel extends StatelessWidget {
  final String titulo;
  final String noticia;
  final double size;

  const _NoticiaWidgetModel({Key key, this.noticia, this.titulo, this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 0,
            child: AutoSizeText(
              titulo,
              style: subtitleTextStyle.copyWith(
                  color: Colors.white, fontSize: size),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            flex: 3,
            child: AutoSizeText(
              noticia,
              overflow: TextOverflow.visible,
              style: subtitleTextStyle.copyWith(
                  color: Colors.white,
                  fontSize: size,
                  fontWeight: FontWeight.w100),
            ),
          )
        ]);
  }
}

class _SlideShowModel with ChangeNotifier {
  double _actual = 0;
  double _total = listadoNoticiasEs.length.toDouble();
  PageController _controller;
  Color _colorPrimario = Colors.orange;
  Color _colorSecundario = Colors.grey;
  double _bulletP = 10.0;
  double _bulletS = 10.0;

  double get actual => this._actual;
  double get total => this._total;

  set actual(double newIndex) {
    _actual = newIndex;
    if (_actual % 1 == 0) notifyListeners();
  }

  set total(double value) {
    _total = value;

    notifyListeners();
  }

  set colorPrimario(Color cp) {
    this._colorPrimario = cp;
  }

  set colorSecundario(Color cp) {
    this._colorSecundario = cp;
  }

  set controller(PageController cp) {
    this._controller = cp;
  }

  void disposed() {
    print('Disposed desde model');
    this._controller.dispose();
  }

  double get bulletP => _bulletP;
  double get bulletS => _bulletS;
  Color get colorPrimario => this._colorPrimario;
  Color get colorSecundario => this._colorSecundario;

  PageController get controller => this._controller;
}

///

/// Caja de Nav

class _CajaNoticiasNav extends StatelessWidget {
  // final PageController pageController;

  // const _CajaNoticiasNav({Key key, this.pageController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //final size = MediaQuery.of(context).size;
    final iconSize = 30.0;
    final prov = Provider.of<_SlideShowModel>(context);
    return Container(
      padding: EdgeInsets.all(30),
      //height: 200,
      width: 200,
      decoration: BoxDecoration(color: Colors.orange),
      child: Column(
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Spacer(),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '${prov.actual + 1}',
                    style: subtitleTextStyle.copyWith(
                        color: Colors.white, fontSize: 30),
                  ),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '/',
                    style: subtitleTextStyle.copyWith(
                        color: Colors.white, fontSize: 30),
                  ),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '${prov.total}',
                    style: subtitleTextStyle.copyWith(
                        color: Colors.white, fontSize: 30),
                  ),
                ),
                Spacer()
              ],
            ),
          ),
          Expanded(
            child: Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                    icon: FaIcon(
                      FontAwesomeIcons.chevronLeft,
                      size: iconSize,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      print(prov.actual - 1);
                      if (prov.actual - 1 >= 0) {
                        prov.controller.animateToPage((prov.actual - 1).toInt(),
                            curve: Curves.easeIn,
                            duration: Duration(milliseconds: 500));
                        prov.actual -= 1;
                      }
                    }),
                Spacer(),
                IconButton(
                    icon: FaIcon(
                      FontAwesomeIcons.chevronRight,
                      size: iconSize,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      if (prov.actual + 1 < listadoNoticiasEs.length) {
                        prov.controller.animateToPage((prov.actual + 1).toInt(),
                            curve: Curves.easeIn,
                            duration: Duration(milliseconds: 500));
                        prov.actual += 1;
                      }
                    }),
              ],
            ),
          )
        ],
      ),
    );
  }
}
