import 'package:finca_pasionaria/src/widgets/sliders/finca/slide_finca_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class Dots extends StatelessWidget {
  final int count;
  Dots(this.count);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, size) {
      return Container(
        color: Colors.black.withOpacity(0.6),
        width: double.infinity,
        height: size.isDesktop ? 50 : 20,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(
              count,
              (i) => _Dot(
                i,
              ),
            )
            //crearDots()
            ),
      );
    });
  }
}

class _Dot extends StatelessWidget {
  final int index;

  const _Dot(this.index);

  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<SlideShowModelFinca>(context);
    double medida;
    Color color;
    if (index - 0.5 <= prov.actual && prov.actual < index + 0.5) {
      medida = prov.bulletP;
      color = prov.colorPrimario;
    } else {
      medida = prov.bulletS;
      color = prov.colorSecundario;
    }
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      height: medida,
      width: medida,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(color: color, shape: BoxShape.circle),
      child: GestureDetector(
        onTap: () {
          prov.actual = index.toDouble();
          prov.control.animateToPage(index,
              duration: Duration(milliseconds: 800), curve: Curves.easeIn);
        },
      ),
    );
  }
}
