import 'package:flutter/material.dart';

class SlideShowModelFinca with ChangeNotifier {
  double _actual = 0;
  Color _colorPrimario = Colors.orange;
  Color _colorSecundario = Colors.grey;
  double _bulletP = 10.0;
  double _bulletS = 10.0;
  PageController _control;
  double get actual => this._actual;
  PageController  get control => this._control;
  set control(PageController c){
    this._control = c;
  }
  set actual(double newIndex) {
    _actual = newIndex;
    notifyListeners();
  }

  set colorPrimario(Color cp) {
    this._colorPrimario = cp;
  }

  set colorSecundario(Color cp) {
    this._colorSecundario = cp;
  }

  double get bulletP => _bulletP;
  double get bulletS => _bulletS;
  Color get colorPrimario => this._colorPrimario;
  Color get colorSecundario => this._colorSecundario;
}
