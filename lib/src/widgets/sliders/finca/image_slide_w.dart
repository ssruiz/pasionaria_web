import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

class ImgSlide extends StatefulWidget {
  final AssetImage imagen;
  const ImgSlide({Key key, this.imagen}) : super(key: key);

  @override
  _ImgSlideState createState() => _ImgSlideState();
}

class _ImgSlideState extends State<ImgSlide>
    with SingleTickerProviderStateMixin {
  final opacity = Tween(
    begin: 0.0,
    end: 1.0,
  );
  AnimationController controller;
  //final controller = AnimationController(vsync: this, duration: Duration(milliseconds: 200));
  //final
  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 850));
    opacity.animate(controller);

    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //controller.forward();
    return FadeIn(
      duration: Duration(milliseconds: 2000),
          child: Container(
        child: Image(image: widget.imagen, fit: BoxFit.cover,),
      ),
    );
    // return Container(
    //   child: FadeTransition(
    //     opacity: opacity.animate(controller),
    //     child: Image(image: widget.imagen, fit: BoxFit.cover,),
    //     // child: FadeInImage(
    //     //   //height: 500,
    //     //   repeat: ImageRepeat.noRepeat,
    //     //   image: imagen,
    //     //   placeholder: AssetImage('assets/img/eclipse.gif'),
    //     //   fit: BoxFit.cover,
    //     // ),
    //   ),
    // );
  }
}