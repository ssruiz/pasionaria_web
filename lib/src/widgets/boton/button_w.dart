import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BotonWidget extends StatelessWidget {
  final Color color;
  final double ancho;
  final double alto;
  final String texto;

  const BotonWidget({this.color = Colors.orange, this.ancho=300,@required this.texto, this.alto});

  @override
  Widget build(BuildContext context) {
    return Container(
      //alignment: Alignment.center,
     padding: EdgeInsets.all(10),
      width: ancho,
      height: alto,
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          shape: BoxShape.rectangle),
      child: Center(
        child: Text(texto, style: GoogleFonts.merriweather(
          fontSize: 20,
          color: Colors.white,
            fontWeight: FontWeight.bold,
            letterSpacing: 3

        ),textAlign: TextAlign.center,),
      ),
    );
  }
}
