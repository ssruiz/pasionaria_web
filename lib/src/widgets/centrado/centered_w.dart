import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class CentradoPagina extends StatelessWidget {
  final Widget child;

  const CentradoPagina({@required this.child});

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, size) {
      return Container(
        //color: Colors.red,
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: size.isDesktop ? 70: 20, vertical: 0),
        child: ConstrainedBox(
            constraints: BoxConstraints(minWidth: 1200),
            child: Container(
                //color: Colors.green,
                child: child)),
      );
    });
  }
}
