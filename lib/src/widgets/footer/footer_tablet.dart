import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/widgets/centrado/centered_w.dart';
import 'package:finca_pasionaria/src/widgets/noticias_widget/noticias_tablet.dart';
import 'package:finca_pasionaria/src/widgets/redes_box/redes_box_w.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:finca_pasionaria/src/providers/rutas_provider.dart';

class FooterTablet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provRutas = Provider.of<RutasProvider>(context);
    return CentradoPagina(
      child: Container(
        height: provRutas.rutaActual != HomeRoute ? 50 : 350,
        width: double.infinity,
        //padding: EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            provRutas.rutaActual == HomeRoute
                ? Expanded(
                    flex: 4,
                    child: NoticiasTablet(),
                  )
                : SizedBox(
                    width: 0,
                  ),
            if( provRutas.rutaActual == HomeRoute) SizedBox(height: 10,),
            RedesBox(),
          ],
        ),
      ),
    );
  }
}
