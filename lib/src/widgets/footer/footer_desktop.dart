import 'package:animate_do/animate_do.dart';
import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/widgets/noticias_widget/noticias_desktop_w.dart';
import 'package:finca_pasionaria/src/widgets/redes_box/redes_box_w.dart';
import 'package:finca_pasionaria/src/widgets/sliders/noticias/sliders_noticia.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class FooterDesktop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provRutas = Provider.of<RutasProvider>(context);
    return Container(
      height: provRutas.rutaActual != HomeRoute ? 50 : 350,

      padding: EdgeInsets.only(left: 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(flex: 1, child: ZoomIn(duration: Duration(milliseconds: 500),child: RedesBox())),
          provRutas.rutaActual == HomeRoute
              ? Flexible(
                  flex: 2,
                  child: NoticiasDesktop(),
                )
              : SizedBox(
                  width: 0,
                ),
        ],
      ),
    );
  }
}

// class _CajaNoticias extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {

//     return Container(
//       padding: EdgeInsets.only(right: 15),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Text(
//             'Novedades',
//             style: GoogleFonts.merriweather(color: Colors.white, fontSize: 24),
//           ),
//           SizedBox(
//             height: 10,
//           ),
//           Text(
//             'Sint mollit consectetur sit aliqua veniam irure irure magna. Elit sint ullamco officia amet dolor enim. Consectetur proident magna eu nostrud mollit non dolore. Cillum velit occaecat deserunt cupidatat id voluptate ad nostrud esse adipisicing reprehenderit qui.',
//             overflow: TextOverflow.clip,
//             style: GoogleFonts.merriweather(
//                 color: Colors.white, fontSize: 18, fontWeight: FontWeight.w100),
//           )
//         ],
//       ),
//     );
//   }
// }

// class _CajaNoticiasNav extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.all(30),
//       height: 200,
//       width: 200,
//       decoration: BoxDecoration(color: Colors.orange),
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Text(
//                 '1',
//                 style:
//                     GoogleFonts.merriweather(color: Colors.white, fontSize: 30),
//               ),
//               Text(
//                 '/',
//                 style:
//                     GoogleFonts.merriweather(color: Colors.white, fontSize: 30),
//               ),
//               Text(
//                 '4',
//                 style:
//                     GoogleFonts.merriweather(color: Colors.white, fontSize: 30),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: 45,
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             // crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               FaIcon(
//                 FontAwesomeIcons.chevronLeft,
//                 size: 30,
//                 color: Colors.white,
//               ),
//               FaIcon(
//                 FontAwesomeIcons.chevronRight,
//                 size: 30,
//                 color: Colors.white,
//               )
//             ],
//           )
//         ],
//       ),
//     );
//   }
// }
