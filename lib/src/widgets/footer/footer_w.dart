import 'package:flutter/material.dart';

import 'package:finca_pasionaria/src/widgets/footer/footer_desktop.dart';
import 'package:finca_pasionaria/src/widgets/footer/footer_tablet.dart';

import 'package:responsive_builder/responsive_builder.dart';

class FooterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (BuildContext context) => FooterDesktop(),
      tablet: (BuildContext context) => FooterTablet(),
      mobile: (BuildContext context) => Container(color: Colors.blue),
      watch: (BuildContext context) => Container(color: Colors.purple),
    );
  }
}