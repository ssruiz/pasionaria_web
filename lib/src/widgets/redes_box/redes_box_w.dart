import 'package:finca_pasionaria/src/util/url_launch_u.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:finca_pasionaria/src/util/hover_extension.dart';

class RedesBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 170,
      height: 50,
      //padding: EdgeInsets.all(5),
      decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: _ItemRedesBox(
              icon: FontAwesomeIcons.facebook,
              lanzar: 'https://facebook.com',
            ).showCursor.moveUpOnHover,
          ),
          Expanded(
            child: _ItemRedesBox(
              icon: FontAwesomeIcons.instagram,
              lanzar: 'https://facebook.com',
            ).showCursor.moveUpOnHover,
          ),
          Expanded(
            child: _ItemRedesBox(
              icon: FontAwesomeIcons.whatsapp,
              lanzar: 'https://facebook.com',
            ).showCursor.moveUpOnHover,
          ),
        ],
      ),
    );
  }
}

class _ItemRedesBox extends StatelessWidget {
  final IconData icon;
  final String lanzar;

  const _ItemRedesBox({@required this.icon, this.lanzar});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: IconButton(
          icon: FaIcon(icon),
          onPressed: () => lanzarUrl(lanzar),
          color: Colors.orange,
          iconSize: 25,
        ),
      ),
    );
  }
}
