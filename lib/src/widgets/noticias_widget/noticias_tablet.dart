import 'package:finca_pasionaria/src/widgets/sliders/noticias/sliders_noticia.dart';
import 'package:flutter/material.dart';

class NoticiasTablet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliderNoticias(
      device: 2,
    );
  }
}
