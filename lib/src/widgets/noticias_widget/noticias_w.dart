import 'package:finca_pasionaria/src/widgets/noticias_widget/noticias_desktop_w.dart';
import 'package:finca_pasionaria/src/widgets/noticias_widget/noticias_tablet.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class NoticiasWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return ScreenTypeLayout.builder(
      desktop: (BuildContext context) => NoticiasDesktop(),
      tablet: (BuildContext context) => NoticiasTablet(),
      mobile: (BuildContext context) => Container(color: Colors.blue),
      watch: (BuildContext context) => Container(color: Colors.purple),
    );
  }
}