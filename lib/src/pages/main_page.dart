import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:provider/provider.dart';

import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:finca_pasionaria/src/providers/app_provider.dart';
import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';

import 'package:finca_pasionaria/src/pages/main_page/main_page_mobile.dart';
import 'package:finca_pasionaria/src/pages/main_page/main_page_desktop.dart';
import 'package:finca_pasionaria/src/pages/main_page/main_page_tablet.dart';
import 'package:finca_pasionaria/src/util/hover_extension.dart';

class MainPage extends StatelessWidget {
  final Widget child;

  const MainPage({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(child: _Background()),
          Positioned.fill(
              child: Container(
            color: Colors.black.withOpacity(0.65),
          )),
          // if (prov != HomeRoute) Positioned(left: 70, child: _TituloPagina()),
          ScreenTypeLayout.builder(
            desktop: (BuildContext context) => MainPageDesktop(
              child: child,
            ),
            tablet: (BuildContext context) => MainPageTablet(
              child: child,
            ),
            mobile: (BuildContext context) => MainPageMobile(
              child: child,
            ),
            //watch: (BuildContext context) => Container(color: Colors.purple),
          ),
        ],
      ),
      drawer: ResponsiveBuilder(builder: (context, sizesInfo) {
        if (sizesInfo.isMobile) {
          return _CrearMenu();
        }
        return Container(width: 0.0, height: 0.0, color: Colors.transparent);
      }),
    );
  }
}

class _CrearMenu extends StatelessWidget {
  const _CrearMenu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final idioma = Provider.of<AplicacionProvider>(context).idioma;
    final styleSelectet = subtitleTextStyle.copyWith(
        color: Colors.orange, fontWeight: FontWeight.bold, fontSize: 15);

    final noSelected = subtitleTextStyle.copyWith(
        color: Colors.white, fontWeight: FontWeight.w100, fontSize: 12);
    return Drawer(
      child: Container(
        color: Colors.black.withOpacity(0.9),
        child: ListView(
          children: [
            DrawerHeader(
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  'Pasionaria',
                  style: titleTextStyle.copyWith(
                      fontSize: 35, color: Colors.white),
                ),
              ),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.orange, Colors.orange[700]])),
            ),
            _ItemDrawer(
              texto: S.of(context).navInicio,
              icono: FontAwesomeIcons.home,
              navigateToPage: HomeRoute,
              index: 0,
            ),
            Divider(
              thickness: 0.5,
              color: Colors.white,
            ),
            _ItemDrawer(
              texto: S.of(context).navFinca,
              icono: FontAwesomeIcons.pagelines,
              index: 1,
              navigateToPage: FincaRoute,
            ),
            Divider(
              thickness: 0.5,
              color: Colors.white,
            ),
            _ItemDrawer(
                texto: S.of(context).navServicio,
                icono: FontAwesomeIcons.conciergeBell,
                index: 2,
                navigateToPage: ServiciosRoute),
            Divider(
              thickness: 0.5,
              color: Colors.white,
            ),
            _ItemDrawer(
              texto: S.of(context).navReserva,
              icono: FontAwesomeIcons.bookOpen,
              index: 3,
              navigateToPage: BookRoute,
            ),
            Divider(
              thickness: 0.5,
              color: Colors.white,
            ),
            SizedBox(
              height: 20,
            ),
            ListTile(
              leading: idioma == 'es'
                  ? FaIcon(FontAwesomeIcons.solidCircle,
                      color: Colors.orange, semanticLabel: 'solid', size: 20)
                  : FaIcon(FontAwesomeIcons.circle,
                      color: Colors.grey, size: 20),
              title: Text(
                'Español',
                style: idioma == 'es' ? styleSelectet : noSelected,
              ),
              onTap: () {
                Provider.of<AplicacionProvider>(context, listen: false).idioma =
                    'es';
              },
            ),
            ListTile(
              leading: idioma == 'en'
                  ? FaIcon(FontAwesomeIcons.solidCircle,
                      color: Colors.orange, size: 20)
                  : FaIcon(FontAwesomeIcons.circle,
                      color: Colors.grey, size: 20),
              title: Text(
                'English',
                style: idioma == 'en' ? styleSelectet : noSelected,
              ),
              onTap: () {
                Provider.of<AplicacionProvider>(context, listen: false).idioma =
                    'en';
              },
            )
          ],
        ),
      ),
    );
  }
}

class _ItemDrawer extends StatelessWidget {
  final IconData icono;
  final int index;
  final String texto;
  final String navigateToPage;
  const _ItemDrawer({
    Key key,
    @required this.icono,
    @required this.index,
    @required this.texto,
    @required this.navigateToPage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provRutas = Provider.of<RutasProvider>(context);
    return ListTile(
      leading: FaIcon(icono,
          color: provRutas.index == index ? Colors.orange : Colors.blueGrey),
      title: Text(
        texto,
        style: subtitleTextStyle.copyWith(
            color: Colors.white, fontSize: 15, fontWeight: FontWeight.w100),
      ),
      onTap: () {
        // Navigator.pop(context);
        provRutas.index = index;
        provRutas.rutaActual = navigateToPage;
      },
    ).colorHoverMenu;
  }
}

class _Background extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FadeIn(
      duration: Duration(milliseconds: 1000),
      child: Image(
        image: AssetImage('assets/img/finca/ext/3.jpg'),
        fit: BoxFit.cover,
      ),
    );
    // return Container(
    //   decoration: BoxDecoration(
    //     image: DecorationImage(
    //       // colorFilter: ColorFilter.mode(
    //       //     Colors.black.withOpacity(0),
    //       //     BlendMode
    //       //         .overlay), //color filter no esta aplicando a web, esperar solucion
    //       //image: AssetImage('assets/img/bg.jpg'),
    //       image: AssetImage('assets/img/finca/ext/3.jpg'),
    //       fit: BoxFit.cover,
    //       //colorFilter: ColorFilter.mode(Colors.black, BlendMode.multiply)
    //     ),
    //     //color:Colors.black.withOpacity(0.6)
    //   ),
    //   child: Container(
    //     decoration: BoxDecoration(color: Colors.black.withOpacity(0.65)),
    //   ),
    // );
  }
}
