import 'package:flutter/material.dart';


import 'package:finca_pasionaria/src/pages/home_page/home_mobile_page.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'package:finca_pasionaria/src/pages/home_page/home_desktop_page.dart';
import 'package:finca_pasionaria/src/pages/home_page/home_tablet_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (BuildContext context) => HomeDeskopPage(),
      tablet: (BuildContext context) => HomeTabletPage(),
      mobile: (BuildContext context) => HomeMobilePage(),
     // watch: (BuildContext context) => Container(color: Colors.purple),
    );
  }
}
