import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:flutter/material.dart';

import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:finca_pasionaria/src/util/hover_extension.dart';
import 'package:provider/provider.dart';

class HomeDeskopPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final animar = Provider.of<RutasProvider>(context).animar;
    return Container(
      padding: EdgeInsets.only(top: 37),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 0,
            child: FadeInLeft(
              animate: animar,
              duration: Duration(milliseconds: 1000),
              delay: Duration(milliseconds: 1000),
              //from: -30,
              child: AutoSizeText(
                'Tenerife',
                style: subtitleTextStyle.copyWith(
                    letterSpacing: 10, fontSize: 30, color: Colors.white),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: FadeInDown(
              animate: animar,
              duration: Duration(milliseconds: 1000),
              delay: Duration(milliseconds: 2000),
              //from: -50,
              child: AutoSizeText(
                'Finca\nPasionaria',
                style: titleTextStyle.copyWith(
                  fontSize: 88,
                  color: Colors.white,
                  fontWeight: FontWeight.w100,
                  height: 1,
                  letterSpacing: 10,
                  wordSpacing: 10,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          FadeInLeft(
            animate: animar,
            duration: Duration(milliseconds: 1000),
            delay: Duration(milliseconds: 3000),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: FlatButton(
                textColor: Colors.white,
                onPressed: () {},
                child: FittedBox(
                  fit: BoxFit.fitHeight,
                  child: Text(S.of(context).btnHome,
                      style: subtitleTextStyle.copyWith(
                        fontSize: 20,
                      )),
                ),
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
              ).colorMouseHover,
            ),
          ),
          Spacer(
            flex: 2,
          )
          //BotonWidget(ancho: 300, alto: 65, texto: 'Ver Habitaciones').showCursor
        ],
      ),
    );
  }
}
