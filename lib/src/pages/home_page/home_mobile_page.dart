import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:finca_pasionaria/src/widgets/centrado/centered_w.dart';
import 'package:flutter/material.dart';

import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:finca_pasionaria/src/util/hover_extension.dart';

class HomeMobilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
 
    //print('Altura: $heigthScreen -Letra: $letraSize');
    return CentradoPagina(
      child: Container(
        //height: heigthScreen * 0.5,
        padding: EdgeInsets.only(top: 37),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              flex: 0,
              child: FadeInLeft(
                duration: Duration(milliseconds: 1000),
                delay: Duration(milliseconds: 1000),
                child: AutoSizeText(
                  'Tenerife',
                  style: subtitleTextStyle.copyWith(
                      letterSpacing: 10, fontSize: 15, color: Colors.white),
                ),
              ),
            ),
            Flexible(
              child: FadeInDown(
                duration: Duration(milliseconds: 1000),
                delay: Duration(milliseconds: 2000),
                child: Container(
                  width: double.infinity,
                  child: AutoSizeText(
                    'Finca\nPasionaria',
                    style: titleTextStyle.copyWith(
                      fontSize: 40,
                      color: Colors.white,
                      fontWeight: FontWeight.w100,
                      height: 1,
                      letterSpacing: 5,
                      wordSpacing: 10,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            FadeInLeft(
              duration: Duration(milliseconds: 1000),
              delay: Duration(milliseconds: 3000),
              child: Container(
                height: 35,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: FlatButton(
                    textColor: Colors.white,
                    onPressed: () {},
                    child: AutoSizeText(
                      S.of(context).btnHome,
                      style: subtitleTextStyle.copyWith(
                        fontSize: 10,
                      ),
                    ),
                    color: Colors.transparent,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                  ).colorMouseHover,
                ),
              ),
            ),

            //BotonWidget(ancho: 300, alto: 65, texto: 'Ver Habitaciones').showCursor
          ],
        ),
      ),
    );
  }
}
