import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/widgets/centrado/centered_w.dart';
import 'package:finca_pasionaria/src/widgets/footer/footer_tablet.dart';
import 'package:finca_pasionaria/src/widgets/navbar/nav_bar_mobile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainPageMobile extends StatelessWidget {
  final Widget child;

  const MainPageMobile({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provRuta = Provider.of<RutasProvider>(context).rutaActual;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        NavBarMobile(),
        SizedBox(
          height: 10,
        ),
        Expanded(
          //height: 300,
          child: Container(
            width: double.infinity,
            child: child,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        provRuta == HomeRoute
            ? Expanded(
                flex: 1,
                child: FooterTablet(),
              )
            : FooterTablet(),
      ],
    );
  }
}
