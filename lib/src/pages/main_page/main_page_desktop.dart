import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/widgets/footer/footer_desktop.dart';
import 'package:finca_pasionaria/src/widgets/navbar/navbar_desktop_w.dart';
import 'package:flutter/material.dart';

import 'package:finca_pasionaria/src/widgets/centrado/centered_w.dart';
import 'package:provider/provider.dart';

class MainPageDesktop extends StatelessWidget {
  final Widget child;

  const MainPageDesktop({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provRuta = Provider.of<RutasProvider>(context).rutaActual;

    return CentradoPagina(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          NavBarDesktop(),
          SizedBox(
            height: 50,
          ),
          Flexible(
              flex: 2, child: SizedBox(width: double.infinity, child: child)),
          // Expanded(flex: 1, child: FooterDesktop())
          SizedBox(
            height: 50,
          ),

          provRuta == HomeRoute
              ? Expanded(flex: 1, child: FooterDesktop())
              : FooterDesktop()
        ],
      ),
    );
  }
}
