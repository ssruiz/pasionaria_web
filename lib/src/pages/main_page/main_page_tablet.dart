import 'package:finca_pasionaria/src/providers/rutas_provider.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/widgets/footer/footer_tablet.dart';
import 'package:finca_pasionaria/src/widgets/navbar/navbar_tablet.dart';
import 'package:flutter/material.dart';

import 'package:finca_pasionaria/src/widgets/centrado/centered_w.dart';
import 'package:provider/provider.dart';

class MainPageTablet extends StatelessWidget {
  final Widget child;

  const MainPageTablet({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provRuta = Provider.of<RutasProvider>(context).rutaActual;
    return CentradoPagina(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            NavBarTablet(),
            SizedBox(
              height: 32,
            ),
            Expanded(
              flex: provRuta == HomeRoute ? 1 : 7,
              child: SizedBox(
                width: double.infinity,
                child: child,
              ),
            ),
            // SizedBox(
            //   height: 50,
            // ),
            FooterTablet()
          ],
        ),
      ),
    );
  }
}
