import 'package:finca_pasionaria/src/pages/finca_page/finca_mobile_page.dart';
import 'package:finca_pasionaria/src/pages/finca_page/finca_tablet_page.dart';
import 'package:flutter/material.dart';

import 'package:finca_pasionaria/src/pages/finca_page/finca_desktop_page.dart';
import 'package:responsive_builder/responsive_builder.dart';

class FincaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return ScreenTypeLayout.builder(
      desktop: (BuildContext context) => FincaDesktopPage(),
      tablet: (BuildContext context) => FincaMobilePage(),
      mobile: (BuildContext context) => FincaMobilePage(),
     // watch: (BuildContext context) => Container(color: Colors.purple),
    );
  }
}