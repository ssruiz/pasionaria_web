import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:finca_pasionaria/src/data/images_finca.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:finca_pasionaria/src/widgets/sliders/finca/dots_slide_w.dart';
import 'package:finca_pasionaria/src/widgets/sliders/finca/image_slide_w.dart';
import 'package:finca_pasionaria/src/widgets/sliders/finca/slide_finca_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class FincaMobilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizeInfo) {
        final tablet = sizeInfo.isTablet;
        return Container(
          padding: EdgeInsets.all(20),
          child: DefaultTabController(
              length: 2,
              child: Column(
                children: [
                  Container(
                    color: Colors.black.withOpacity(0.5),
                    child: TabBar(tabs: <Widget>[
                      Tab(
                        child: Text(S.of(context).fincaTab1,
                            style: subtitleTextStyle.copyWith(
                                color: Colors.white)),
                      ),
                      Tab(
                        child: Text(S.of(context).fincaTab2,
                            style: subtitleTextStyle.copyWith(
                                color: Colors.white)),
                      ),
                    ]),
                  ),
                  Expanded(
                    child: Container(
                      child: TabBarView(children: <Widget>[
                        _SlideFinca(tipo: 1, tablet: tablet),
                        _SlideFinca(
                          tipo: 2,
                          tablet: tablet,
                        ),
                      ]),
                    ),
                  ),
                ],
              )),
        );
      },
    );
  }
}

class _SlideFinca extends StatefulWidget {
  final int tipo;
  final bool tablet;

  const _SlideFinca({Key key, @required this.tipo, this.tablet})
      : super(key: key);
  @override
  __SlideFincaState createState() => __SlideFincaState();
}

class __SlideFincaState extends State<_SlideFinca> {
  final PageController controller = new PageController();
  List<AssetImage> listadoImagenes;
  @override
  void initState() {
    listadoImagenes = widget.tipo == 1 ? listadoExterior : listadoInterior;

    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final tituloSeccion = widget.tipo == 1
        ? S.of(context).fincaExteriorTitulo
        : S.of(context).fincaInteriorTitulo;
    return ChangeNotifierProvider(
      create: (_) => new SlideShowModelFinca(),
      child: Consumer<SlideShowModelFinca>(builder: (ctx, val, child) {
        val.control = controller;
        controller.addListener(() {
          val.actual = controller.page;
        });
        return Container(
          child: Column(
            children: [
              Expanded(
                flex: 2,
                child: Stack(
                  fit: StackFit.loose,
                  alignment: Alignment.bottomCenter,
                  children: [
                    PageView.builder(
                        physics: BouncingScrollPhysics(),
                        controller: controller,
                        itemCount: listadoImagenes.length,
                        itemBuilder: (context, i) {
                          return ImgSlide(
                            imagen: listadoImagenes[i],
                          );
                        }),
                    Dots(
                      listadoImagenes.length,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(20),
                      decoration:
                          BoxDecoration(color: Colors.black.withOpacity(0.5)),
                      child: _DescripcionFotos(
                        titulo: tituloSeccion,
                        texto1:
                            'Esse Lorem deserunt id fugiat culpa mollit et. Aute ullamco tempor aute aliqua proident non ut eu qui. Cupidatat irure non culpa velit quis ex id ut reprehenderit nulla adipisicing cupidatat dolore laboris. Laboris anim do elit ullamco laboris officia sit. Aliqua exercitation fugiat deserunt deserunt laboris. Do culpa ipsum laborum nulla.\n',
                        texto2:
                            'Esse Lorem deserunt id fugiat culpa mollit et. Aute ullamco tempor aute aliqua proident non ut eu qui. Cupidatat irure non culpa velit quis ex id ut reprehenderit nulla.',
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}

class _DescripcionFotos extends StatelessWidget {
  final String titulo;
  final String texto1;
  final String texto2;

  const _DescripcionFotos({
    Key key,
    this.texto1,
    this.texto2,
    this.titulo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          titulo,
          style: subtitleTextStyle.copyWith(fontSize: 12, color: Colors.white),
        ),
        Divider(
          thickness: 2,
          color: Colors.grey[300],
        ),
        
        Flexible(
          child: AutoSizeText(
            texto1,
            textAlign: TextAlign.justify,
            style: subtitleTextStyle.copyWith(
                wordSpacing: 1,
                fontWeight: FontWeight.w100,
                fontSize: 10,
                color: Colors.white),
          ),
        ),
      ],
    );
  }
}

class _Dots extends StatelessWidget {
  final int count;
  _Dots(this.count);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      color: Colors.black.withOpacity(0.6),
      width: double.infinity,
      //height: 50,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(count, (i) => _Dot(i))
          //crearDots()
          ),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;

  const _Dot(this.index);

  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<_SlideShowModel>(context);
    double medida;
    Color color;
    if (index - 0.5 <= prov.actual && prov.actual < index + 0.5) {
      medida = prov._bulletP;
      color = prov.colorPrimario;
    } else {
      medida = prov._bulletS;
      color = prov.colorSecundario;
    }
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      height: medida,
      width: medida,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(color: color, shape: BoxShape.circle),
    );
  }
}

class _SlideShowModel with ChangeNotifier {
  double _actual = 0;
  Color _colorPrimario = Colors.orange;
  Color _colorSecundario = Colors.grey;
  double _bulletP = 10.0;
  double _bulletS = 10.0;

  double get actual => this._actual;

  set actual(double newIndex) {
    _actual = newIndex;
    notifyListeners();
  }

  set colorPrimario(Color cp) {
    this._colorPrimario = cp;
  }

  set colorSecundario(Color cp) {
    this._colorSecundario = cp;
  }

  double get bulletP => _bulletP;
  double get bulletS => _bulletS;
  Color get colorPrimario => this._colorPrimario;
  Color get colorSecundario => this._colorSecundario;
}
