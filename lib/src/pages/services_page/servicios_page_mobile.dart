import 'dart:ui';

import 'package:finca_pasionaria/src/data/listado_servicios.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class ServiciosPageMobile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // return PageView.builder(
    //   itemCount: 2,

    //   physics: BouncingScrollPhysics(),
    //   itemBuilder: (context, i) {
    //     return Container(
    //       margin: EdgeInsets.all(50),
    //       color: Colors.red,
    //     );
    //   },
    // );
    return _ScrollServicios();
  }
}

class _ScrollServicios extends StatefulWidget {
  const _ScrollServicios({
    Key key,
  }) : super(key: key);

  @override
  __ScrollServiciosState createState() => __ScrollServiciosState();
}

class __ScrollServiciosState extends State<_ScrollServicios> {
  final scrollControll = ScrollController();

  void onListen() {
    // print('scrooll: ${scrollControll.offset}');
    setState(() {});
  }

  @override
  void initState() {
    scrollControll.addListener(onListen);

    super.initState();
  }

  @override
  void dispose() {
    scrollControll.removeListener(onListen);
    scrollControll.dispose();

    super.dispose();
  }

  final itemSize = 500.0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return _ServicioCard(
            index: index,
          );
        },
        viewportFraction: 0.8,
        scale: 0.9,
        itemCount: listadoImgServicios.length,
        pagination: new SwiperPagination(),
        layout: SwiperLayout.CUSTOM,
        customLayoutOption: new CustomLayoutOption(startIndex: 0, stateCount: 3)
          ..addTranslate([
            new Offset(-370.0, -40.0),
            new Offset(0.0, 0.0),
            new Offset(370.0, -40.0)
          ])
          ..addOpacity([0.5, 1, 0.5]),
        itemWidth: 350,
        fade: 2.0,
        //layout: SwiperLayout.STACK,
        // control: new SwiperControl(),
      ),
    );
    // return CustomScrollView(
    //   controller: scrollControll,
    //   physics: BouncingScrollPhysics(),
    //   scrollDirection: Axis.horizontal,
    //   anchor: 0.0,
    //   slivers: <Widget>[
    //     SliverList(
    //       delegate: SliverChildBuilderDelegate(
    //         (context, index) {
    //           final itemPosition = index * itemSize;
    //           final dif = scrollControll.offset - itemPosition;
    //           final percent = 1 - (dif / itemSize);
    //           double op = percent < 0 ? 0 : percent;
    //           double scale = percent;
    //           //if (op >= 2) op = op - 1.5;
    //           if (op > 1) op = 1;
    //           if (scale > 1.0) scale = 1.0;
    //           //if (index == 1) print('asd: $percent');
    //           return Opacity(
    //             opacity: op,
    //             child: Transform(
    //               alignment: Alignment.center,
    //               transform: Matrix4.identity()..scale(1.0, scale),
    //               child: _ServicioCard(
    //                 index: index,
    //               ),
    //             ),
    //           );
    //         },
    //         childCount: listadoImgServicios.length,
    //       ),
    //     ),
    //   ],
    // );
  }
}

class _ServicioCard extends StatelessWidget {
  const _ServicioCard({
    Key key,
    this.index,
  }) : super(key: key);
  final int index;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      //padding: EdgeInsets.only(right: 5),
      margin: EdgeInsets.only(right: 20),
      child: Card(
        color: Colors.black.withOpacity(0.5),
        child: Column(
          children: [
            Expanded(
              child: Container(
                child: Image(
                  // height: 200,
                  fit: BoxFit.cover,
                  image: listadoImgServicios[index],
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(top: 20, left: 30, right: 30),
                color: Colors.orange.withOpacity(0.5),
                child: Column(
                  children: [
                    Flexible(
                      child: Container(
                        child: Text(
                          'Servicio 1',
                          style: subtitleTextStyle.copyWith(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                    Divider(
                      thickness: 2,
                      color: Colors.white,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Expanded(
                      flex: 2,
                      child: SingleChildScrollView(
                        
                        child: Text(
                          'Excepteur ea quis minim sunt sit dolore anim adipisicing laborum. Ut sunt dolore non consequat nulla sunt est Lorem in pariatur ad fugiat aute exercitation. Consequat eiusmod nisi commodo esse irure quis. Culpa cillum ullamco commodo commodo nostrud commodo.',
                          style: subtitleTextStyle.copyWith(
                            color: Colors.white,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
