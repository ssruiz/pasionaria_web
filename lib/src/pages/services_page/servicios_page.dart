import 'package:finca_pasionaria/src/pages/services_page/servicios_page_desktop.dart';
import 'package:finca_pasionaria/src/pages/services_page/servicios_page_mobile.dart';
import 'package:finca_pasionaria/src/pages/services_page/servicios_page_tablet.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ServiciosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ScreenTypeLayout.builder(
        desktop: (context) => ServiciosPageDesktop(),
        tablet: (context) => ServiciosPageMobile(),
        mobile: (context) => ServiciosPageMobile(),
      ),
    );
  }
}
