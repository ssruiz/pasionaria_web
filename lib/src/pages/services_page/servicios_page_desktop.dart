import 'dart:ui';

import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:finca_pasionaria/src/data/listado_servicios.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:flutter/material.dart';

class ServiciosPageDesktop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(child: FadeInRight( child: _CardServicio(img: listadoImgServicios[0]))),
          SizedBox(
            width: 100,
          ),
          Flexible(child: FadeInRight(delay: Duration(milliseconds: 1000),child: _CardServicio(img: listadoImgServicios[1]))),
          SizedBox(
            width: 100,
          ),

          Flexible(child: FadeInRight(delay: Duration(milliseconds: 2000),child: _CardServicio(img: listadoImgServicios[2]))),

          // _CardServicio(),
        ],
      ),
    );
  }
}

class _CardServicio extends StatelessWidget {
  final AssetImage img;
  const _CardServicio({
    Key key,
    this.img,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 450,
      child: Column(
        children: [
          Flexible(
            child: Container(
              width: double.infinity,
              child: Image(
                image: img,
                height: 500,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Expanded(
            child: ClipRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                child: Container(
                  padding: EdgeInsets.all(30),
                  color: Colors.orange.withOpacity(0.4),
                  child: Column(
                    children: [
                      Expanded(
                        flex: 0,
                        child: Container(
                          child: AutoSizeText(
                            'Servicio 1',
                            style: subtitleTextStyle.copyWith(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 2,
                        color: Colors.white,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: Container(
                          child: AutoSizeText(
                            'Excepteur ea quis minim sunt sit dolore anim adipisicing laborum. Ut sunt dolore non consequat nulla sunt est Lorem in pariatur ad fugiat aute exercitation. Consequat eiusmod nisi commodo esse irure quis. Culpa cillum ullamco commodo commodo nostrud commodo.',
                            style: subtitleTextStyle.copyWith(
                              color: Colors.white,
                              fontSize: 15,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
