import 'package:finca_pasionaria/src/providers/form_validation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'package:finca_pasionaria/src/pages/book_page/book_desktop_page.dart';
import 'package:finca_pasionaria/src/pages/book_page/book_mobile_page.dart';
import 'package:finca_pasionaria/src/pages/book_page/book_tablet_page.dart';

class BookPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => FormValidationProvider(),
      child: Container(
        child: ScreenTypeLayout.builder(
          desktop: (context) => BookDesktopPage(),
          tablet: (context) => BookMobilePage(),
          mobile: (context) => BookMobilePage(),
        ),
      ),
    );
  }
}
