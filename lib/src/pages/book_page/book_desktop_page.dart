import 'package:animate_do/animate_do.dart';
import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:finca_pasionaria/src/providers/form_validation.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:finca_pasionaria/src/widgets/inputs/input_form_w.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class BookDesktopPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: _ImgBook(),
          ),
          Expanded(
            flex: 2,
            child: Container(
              height: double.infinity,
              color: Colors.white,
              child: _Formulario(),
            ),
          )
        ],
      ),
    );
  }
}

class _ImgBook extends StatelessWidget {
  const _ImgBook({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SlideInDown(
      duration: Duration(milliseconds: 1000),
      from: 2000,
      child: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/img/book.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          color: Colors.black.withOpacity(0.5),
        ),
      ),
    );
  }
}

class _Formulario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SlideInDown(
      duration: Duration(milliseconds: 1000),
      delay: Duration(milliseconds: 1000),
      from: -1000,
      child: Container(
        padding: EdgeInsets.only(left: 50, right: 50, top: 50),
        child: Row(
          children: [
            Expanded(
              child: DatosFormulario(),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Reserva(),
            ),
          ],
        ),
      ),
    );
  }
}

