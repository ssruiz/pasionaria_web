import 'package:animate_do/animate_do.dart';
import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:finca_pasionaria/src/providers/form_validation.dart';
import 'package:finca_pasionaria/src/util/fuentes.dart';
import 'package:finca_pasionaria/src/widgets/inputs/input_form_w.dart';
import 'package:flutter/material.dart';
import 'package:finca_pasionaria/src/widgets/centrado/centered_w.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class BookMobilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CentradoPagina(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),
        ),
        //color: Colors.blue,
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: FadeInDown(
                duration: Duration(milliseconds: 1000),
                child: _HeaderForm(),
              ),
            ),
            Expanded(
              flex: 4,
              child: FadeInLeft(
                duration: Duration(milliseconds: 1000),
                delay: Duration(milliseconds: 1000),
                child: _Formulario(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _Formulario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(15),
          bottomRight: Radius.circular(15),
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 3.0,
            offset: Offset(3, 3),
            spreadRadius: 3.0,
          ),
        ],
      ),
      child: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: _Datos(),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          BotonSubmit()
        ],
      ),
    );
  }
}

class _HeaderForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          child: Image.asset(
            'assets/img/book.jpg',
            fit: BoxFit.cover,
          ),
        ),
        Container(
          color: Colors.black.withOpacity(0.5),
        ),
        Container(
          child: Center(
            child: Text(
              'Solicita un registro',
              style: subtitleTextStyle.copyWith(
                color: Colors.white,
                fontSize: 30,
              ),
            ),
          ),
        )
      ],
    );
  }
}

class _Datos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              S.of(context).formularioDatosTitulo,
              style: subtitleTextStyle.copyWith(
                color: Colors.black87,
                fontSize: 25,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  InputNombre(),
                  InputApellido(),
                  InputEmail(),
                  InputMovil(),
                  SizedBox(
                    height: 10,
                  ),
                  _Reserva()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _Reserva extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              S.of(context).formularioReservaTitulo,
              style: subtitleTextStyle.copyWith(
                color: Colors.black87,
                fontSize: 25,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          _DatePicker(
            fecha: prov.fechaInicio,
            icono: FontAwesomeIcons.calendarCheck,
            texto: S.of(context).formularioLabelFechaI,
          ),
          SizedBox(
            height: 10,
          ),
          _DatePicker(
            fecha: prov.fechaFin,
            icono: FontAwesomeIcons.calendarMinus,
            texto: S.of(context).formularioLabelFechaS,
          ),
          InputNroPersonas()
        ],
      ),
    );
  }
}

class _DatePicker extends StatefulWidget {
  final fecha;
  final IconData icono;
  final String texto;
  const _DatePicker({Key key, this.fecha, this.icono, this.texto})
      : super(key: key);

  @override
  __DatePickerState createState() => __DatePickerState();
}

class __DatePickerState extends State<_DatePicker> {
  TextEditingController cont = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    cont?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<FormValidationProvider>(context);

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 15,
      ),
      child: TextField(
        controller: cont,
        decoration: InputDecoration(
            labelText: widget.texto,
            icon: FaIcon(
              FontAwesomeIcons.calendar,
            )),
        onTap: () {
          showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(DateTime.now().year),
                  lastDate: DateTime(2100))
              .then((value) {
            if (value != null) {
              prov.changeFechaInicio(value);
              cont.text = '${DateFormat("dd-MM-yyyy").format(value)}';
              setState(() {});
            }
          });
        },
      ),
    );
  }
}
