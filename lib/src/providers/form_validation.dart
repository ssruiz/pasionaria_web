import 'package:finca_pasionaria/src/util/validators/validation_item.dart';
import 'package:flutter/material.dart';

class FormValidationProvider with ChangeNotifier {
  // DATOS
  ValidationItem _nombre = ValidationItem(null, false);
  ValidationItem _apellido = ValidationItem(null, false);
  ValidationItem _email = ValidationItem(null, false);
  ValidationItem _movil = ValidationItem(null, false);
  // Reserva
  DateTime _fechaInicio = DateTime.now();
  DateTime _fechaFin = DateTime.now();
  ValidationItem _nroPersonas = ValidationItem(null, false);

  // Getters Datos
  ValidationItem get nombre => _nombre;
  ValidationItem get apellido => _apellido;
  ValidationItem get email => _email;
  ValidationItem get movil => _movil;
  ValidationItem get nroPersonas => _nroPersonas;

  //Getters Rreserva
  DateTime get fechaInicio => _fechaInicio;
  DateTime get fechaFin => _fechaFin;

  // Setters Datos
  void changeNombre(String value) {
    if (value.length > 3)
      _nombre = ValidationItem(value, false);
    else
      _nombre = ValidationItem(null, true);

    notifyListeners();
  }

  void changeApellido(String value) {
    if (value.length > 3)
      _apellido = ValidationItem(value, false);
    else
      _apellido = ValidationItem(null, true);

    notifyListeners();
  }

  void changeEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp validate = RegExp(pattern);

    if (validate.hasMatch(value))
      _email = ValidationItem(value, false);
    else
      _email = ValidationItem(null, true);

    notifyListeners();
  }

  void changeMovil(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);

    if (value.length == 0)
      _movil = ValidationItem(null, true);
    else if (!regExp.hasMatch(value))
      _movil = ValidationItem(null, true);
    else
      _movil = ValidationItem(value, false);
    notifyListeners();
  }

  // Setters Reserva
  void changeFechaInicio(DateTime value) {
    _fechaInicio = value;

    notifyListeners();
  }

  void changeFechaFin(DateTime value) {
    _fechaFin = value;

    notifyListeners();
  }

  void changeNroPersonas(String value) {
    try {
      final number = double.tryParse(value);
      if (number != null)
        _nroPersonas = ValidationItem(value, false);
      else
        _nroPersonas = ValidationItem(null, true);
    } catch (e) {
       _nroPersonas = ValidationItem(null, true);
    }

    notifyListeners();
  }

  bool get validateForm {
    if (_nombre.value != null &&
        _apellido.value != null &&
        _email.value != null &&
        _movil.value != null &&
        validarFechas()) {
      return true;
    }
    return false;
  }

  bool validarFechas() {
    if (_fechaInicio.isAfter(_fechaFin)) return false;

    return true;
  }
}
