import 'package:finca_pasionaria/generated/l10n.dart';
import 'package:flutter/cupertino.dart';

class AplicacionProvider with ChangeNotifier{

  String _idioma = 'es';

  get idioma => this._idioma;

  set idioma(String value){
    _idioma=value;
   // print('idioma: $value');
    S.load(Locale('$value'));
    notifyListeners();
  }
}