import 'package:finca_pasionaria/locator.dart';
import 'package:finca_pasionaria/src/routes/routes.dart';
import 'package:finca_pasionaria/src/services/navigation_service.dart';
import 'package:flutter/cupertino.dart';


class RutasProvider with ChangeNotifier {
  String _rutaActual = HomeRoute;
  int _index = 0;
  bool _animar = true;
  //GetIt locatorService = new locator<NavigationService>();
  get rutaActual => this._rutaActual;
  get index => this._index;
  get animar => this._animar;

  set rutaActual(String value) {
    _rutaActual = value;
    
     locator<NavigationService>().navigateTo(value);
    
    notifyListeners();
  }

  set index (int value) {
    _index = value;
  }
  
  set animar(bool value){
    _animar = value;
    //notifyListeners();
  }
}
